import os
from random import randint
from generic.config.config_loader import ConfigLoader
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class PetStore:
    EnvConfig = ConfigLoader.load_config()
    URL = EnvConfig.url
    SIGN_IN_BTN = (By.CSS_SELECTOR, '#MenuContent > a:nth-child(3)')
    LOGIN_PAGE_TEXT = (By.CSS_SELECTOR, "#Catalog > form > p:nth-child(1)")
    USER_ID_INPUT = (By.CSS_SELECTOR, "input[name ='username']")
    REGISTER_NOW_BTN = (By.CSS_SELECTOR, "#Catalog > a")

    def __init__(self, browser):
        self.browser = browser
        self.wait = WebDriverWait(browser, 10)

    def load_url(self):
        self.browser.get(self.URL)

    def wait_for_element(self, element):
        self.wait.until(EC.presence_of_element_located(element))

    def click_in_sign_in(self):
        sign_in_btn = self.browser.find_element(*self.SIGN_IN_BTN)
        sign_in_btn.click()
        self.wait_for_element(self.LOGIN_PAGE_TEXT)

    def navigate_to_registration_page(self):
        self.load_url()
        self.click_in_sign_in()
        register_btn = self.browser.find_element(*self.REGISTER_NOW_BTN)
        register_btn.click()
        self.wait_for_element(self.USER_ID_INPUT)

    def get_login_text(self):
        login_text = self.browser.find_element(*self.LOGIN_PAGE_TEXT).text
        return login_text

    def generate_user(self):
        random = str(randint(12345, 99999))
        env = os.getenv('env')
        return env + random

    def generate_pass(self):
        random_password = "Test."
        random = str(randint(12345, 99999))
        return random_password + random